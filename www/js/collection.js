angular.module('starter.collection', [])
.service('$localstorage',['$window',function($window){
	var localstorage = {
			set : function(key,value){
				$window.localStorage[key] = value;
			},
			get : function(key,defaultValue){
				return $window.localStorage[key] || defaultValue;
			},
			setObj : function(key,value){
				$window.localStorage[key] = JSON.stringify(value);
			},
			getObj : function(key){
				return JSON.parse($window.localStorage[key] || '{}');
			},
			ask : function(key){
				var login_bool = this.get(key) != null 
				&& this.get(key) != 'null' 
				&& this.get(key).length > 0 ? true : false;
				
				return login_bool;
			}
	};
	return localstorage;
}])
.service('versionChk',function($http,ApiEndpoint){
	return {
		google_store : function(){
			return $http.get(ApiEndpoint.google +"/details?id=com.bogoga");
		}
	}
})
.service('LoginChk',function($localstorage,$http, ApiEndpoint){
	return {
		get : function(val){
			console.log('LoginChk', val);
			var params = {
					email : val.user_email,
					password : val.user_pw
			};
			console.log();
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/login/login_info",
			    data: $.param({
			    	email : params.email,
			    	password : params.password
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		biz_chk : function(email){
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/service/marketChk",
			    data: $.param({
			    	email : email
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		}
	}
})
.service('ShopUtile',function($http,ApiEndpoint){
	return {
		point : function(dbObj){
			var params = dbObj;
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +params.url,
			    data: $.param({
			    	email : params.email,
			    	shop_id : params.shop_id,
			    	sel_col : params.sel_col
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		zzim : function(dbObj){
			var params = dbObj;
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/service/zzimOn",
			    data: $.param({
			    	email : params.email,
			    	shop_id : params.shop_id
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		zzim_len: function(dbObj){
			var params = dbObj;
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/service/zzim_len",
			    data: $.param({
			    	email : params.email
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		zzim_del: function(dbObj) {
			var params = dbObj;
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/service/zzimList_del",
			    data: $.param({
			    	email : params.email,
			    	shop_id : params.shop_id
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		}
	}
})
.service('_Utile',function($state,$ionicHistory){
	var utile = {
			getRandom : function(max){
				return Math.floor((Math.random()*max)+1);
			},
			kwChange : function(kw){
				var kwArr = [];
				var sumKw = "";
				kwArr = kw.split(",");
				for(var i in kwArr){
					sumKw += "#"+kwArr[i];
				}
				return sumKw;
			},
			back : function(){
				console.log($ionicHistory.backView());
				var stateParams = "";
				if($ionicHistory.backView().stateParams){
					stateParams = $ionicHistory.backView().stateParams.param
								?$ionicHistory.backView().stateParams.param:"";
				}
				$state.go($ionicHistory.backView().stateName,{param:stateParams});
				$ionicHistory.clearCache();
			},
			trim : function(s){
				var t = "";
				var from_pos = to_pos = 0;

				for (i = 0; i < s.length; i++) {
					if (s.charAt(i) == ' ')
						continue;
					else {
						from_pos = i;
						break;
					}
				}

				for (i = s.length; i >= 0; i--) {
					if (s.charAt(i - 1) == ' ')
						continue;
					else {
						to_pos = i;
						break;
					}
				}

				t = s.substring(from_pos, to_pos);
				return t;
			}
	};
	
	return utile;
})
.service('UserService', function() {
	var setUser = function(user_data) {
		window.localStorage.starter_facebook_user = JSON.stringify(user_data);
	};

	var getUser = function(){
		return JSON.parse(window.localStorage.starter_facebook_user || '{}');
	};

	return {
	  getUser: getUser,
	  setUser: setUser
	};
})
.service('CallService',function(){
	var local_number = {
			"02" : "서울특별시",
			"031" : "경기도",
			"032" : "인천광역시",
			"033" : "강원도",
			"041" : "	충청남도",
			"042" : "대전광역시",
			"043" : "충청북도",
			"044" : "세종특별자치시",
			"051" : "부산광역시",
			"052" : "울산광역시",
			"053" : "대구광역시",
			"054" : "경상북도",
			"055" : "경상남도",
			"061" : "전라남도",
			"062" : "광주광역시",
			"063" : "전라북도",
			"064" : "제주특별자치도"
	};
	var cell_number = ["010","011","012","015","016","017","018","019"];
	

	return {
		local : local_number,
		cell : cell_number
	};
})
.service('Assent', function($ionicPopup) {

	return {
		action : function($scope,kind){

			console.log(kind);
			var postPopup = $ionicPopup.show({
				templateUrl: 'templates/popup/assent.html',
			    title: '약관',
			    cssClass: 'assent-popup',
			    scope: $scope,
			    buttons: [
			              { text: 'Cancel' }
			              ]
			});
			
			/*카테고리 탭 영역 */
			  var default_tab_path = 'templates/popup/inc/';
			  $scope.tabList = [
			                    {tabTitle : '서비스 이용약관',active:false,url:'access.html'},
			                    {tabTitle : '개인정보 취급방침',active:false,url:'personal.html'}
			                    ];
			  $scope.tab = {
					  url : ""
			  }

			  for(var i in $scope.tabList){
				  if($scope.tabList[i].url == kind+'.html'){
					  $scope.tab.url = default_tab_path+$scope.tabList[i].url;
					  $scope.tabList[i].active = true;
					  console.log($scope.tab.url);
					  break;
				  };
			  };
			  $scope.category_tab = function(item){
				  console.log(item);
				  resetActive();

				  if(!item.active){
					  $scope.tab.url = default_tab_path+item.url;
					  item.active = !item.acvite;
				  };
				  
			  };
			  
			  function resetActive(){
				  for(var i in $scope.tabList){
					  $scope.tabList[i].active = false;
				  }
			  };
		
		}
	};
})
.service('Post', function($http,$ionicPopup,$ionicLoading,$timeout,toaster,ApiEndpoint) {
	return {
		action : function($scope){
			var postPopup = $ionicPopup.show({
				templateUrl: 'templates/popup/post.html',
			    title: '우편번호검색',
			    cssClass: 'post-popup',
			    scope: $scope,
			    buttons: [
			              { text: 'Cancel' }
			              ]
			});
			
			//angular.element(document.querySelector(".loadMore")).css('none_block');
			$scope.loadMore = function(){};
			$scope.display_none = {'display': 'none'};
			$scope.postSelect = function(item){
				console.log(item);
				$scope.data.post_no = item.split("|")[0];
				$scope.data.addr = item.split("|")[1];
				postPopup.close();
			}
			  
			$scope.postSearch = function(){
				console.log("addr_txt - "+this.addr_txt);
				$scope.addr_txt = this.addr_txt!=null ?this.addr_txt:"";  //검색 초기화
				$scope.all = null; //데이터 초기화
				var page_index = 0;
				var loadMoreChk = true;
				
				$scope.noMoreItemsAvailable = false;
				
				$scope.loadMore = loadMoreFn;
				
				loadMoreFn();
				
				function loadMoreFn(){
					$scope.loading = //로딩화면 생성
						$ionicLoading.show({
							content: 'Loading',
							animation: 'fade-in',
							showBackdrop: true,
							maxWidth: 200,
							showDelay: 0
						});
					page_index ++;
					
					  var  post_allObj = {
							  "data" : $scope.addr_txt,
							  "page" : page_index
					  };

					  if(loadMoreChk){
						  return $http.get(ApiEndpoint.root_url
								  +"/api/postGet?data="
								  +post_allObj.data+"&page="
								  +post_allObj.page)
								  .then(function(res){
									  console.log("post 검색 결과");
									  console.log(res);
									  console.log(res.data.length);
									  if(res.data.length > 0){
										  $timeout(function () {
								    		    $ionicLoading.hide();
								    		    if($scope.all != null){
													  for(i in res.data){
														  $scope.all.push(res.data[i]);
													  }
												  }else{
													  $scope.all = res.data;
												  }
												  if ( $scope.all.length == 99 ) {
													  $scope.noMoreItemsAvailable = true;
												  }
												  $scope.$broadcast('scroll.infiniteScrollComplete');
										  }, 1000);
									  }else{
										  $timeout(function () {
											  loadMoreChk = false;
											  toaster.pop('error'
									    				, "알림창"
									    				, "검색 결과가 없습니다."
									    				, 2000);
											  $ionicLoading.hide();
										  });
										  
									  }
									  
								  });
					  };
				};
			};	  
		}
	};
})
.service('CategoryPop',function($ionicPopup,$http,$timeout,ApiEndpoint){
	
	return {
		action : function($scope){
			var categoryPopup = $ionicPopup.show({
				templateUrl: 'templates/popup/category_list.html',
			    title: '카테고리선택',
			    cssClass: 'category-popup',
			    scope: $scope
			});

			$http.get(ApiEndpoint.url +"/service/categoryList")
			.then(function(res){
				console.log("category_list");
				console.log(res.data);

				$scope.category_list = res.data;
				
			});
			
			$scope.popup_close = function(){
				categoryPopup.close();
			};
			
			$scope.categoryAction = function(item){
				console.log(item);
				$scope.data.code_cd = item.code_cd;
				$scope.data.code_nm = item.code_nm;
				categoryPopup.close();
			}
		}
	}
})
.service('Subway',function($ionicPopup,$http,$ionicLoading,$timeout,ApiEndpoint){
	return {
		action : function($scope){
			
			var subwayPopup = $ionicPopup.show({
				templateUrl: 'templates/popup/subway.html',
			    title: '수도권지하철 검색',
			    cssClass: 'subway-popup',
			    scope: $scope,
			    buttons: [
			              { text: 'Cancel' }
			              ]
			});
			
			var subway_line = {
					'1호선' : '1',
					'2호선' : '2',
					'3호선' : '3',
					'4호선' : '4',
					'5호선' : '5',
					'6호선' : '6',
					'7호선' : '7',
					'8호선' : '8',
					'9호선' : '9',
					'인천1호선' : 'I',
					'경의/중앙선' : 'K',
					'분당선' : 'B',
					'공항철도' : 'A',
					'경춘선' : 'G',
					'신분당선' : 'S',
					'수인선' : 'SU',
			}
			$scope.line_code = subway_line;
			var subway_key = "4b6b6d4d6373657239305a6b415468"; // 보안 작업  해야함
			var subline_url = "SearchSTNBySubwayLineService";
			var subname_url = "SearchInfoBySubwayNameService";		
			
			$scope.lineClick = function($event,line_code){
				$scope.loading = //로딩화면 생성
					$ionicLoading.show({
						content: 'Loading',
						animation: 'fade-in',
						showBackdrop: true,
						maxWidth: 200,
						showDelay: 0
					});
				
				angular.element('.subway').find('.line_code span').removeClass('on');
				angular.element($event.target).addClass('on');
				
				var subway_allObj = {
						key : subway_key,
						subline : subline_url,
						line_code : line_code
				};
				$http.get(ApiEndpoint.subway 
						+"/"+subway_allObj.key
						+"/json/"
						+subway_allObj.subline
						+"/1/128/"
						+subway_allObj.line_code)
						.then(function(res){
							console.log(res.data);
							
							var result = res.data.SearchSTNBySubwayLineService.RESULT;
							var row = res.data.SearchSTNBySubwayLineService.row;
							$timeout(function () {
				    		    $ionicLoading.hide();
				    		}, 1000);
							$scope.subway_row = row;
						});
				
			};
			
			$scope.subwayClick = function(item){
				console.log(item);
				item = JSON.parse(item);
				console.log(item);
				$scope.data.station_nm = item.STATION_NM;
				$scope.data.station_cd = item.STATION_CD;
				$scope.data.line_num = item.LINE_NUM;
				$scope.data.fr_code = item.FR_CODE;
				subwayPopup.close();
			}
		}
	};
})

;