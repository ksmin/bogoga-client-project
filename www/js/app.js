// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','starter.collection','ngOpenFB','toaster','ngCordova','ionic-datepicker'])
.constant('ApiEndpoint', {
	url:'http://52.79.78.117/difi',
	root_url:'http://52.79.78.117',
	subway: 'http://openapi.seoul.go.kr:8088',
	map : 'http://maps.googleapis.com/maps/api/geocode/json',
	post : 'http://biz.epost.go.kr/KpostPortal',
	google : 'https://play.google.com/store/apps',
	
	url12 :'/local',
	subway12 : '/subway',
	map12 : '/maps',
	post12 : '/KpostPortal',
	
	server : "http://52.79.78.117/difi",
	local :"http://192.168.0.105:9090/difi",
	local12 :"/local"

})
.run(function($ionicPlatform,$state,$ionicPopup,$ionicHistory,$localstorage,versionChk,ngFB,ApiEndpoint) {  //페이스북 연동을 위해 ngFB추가
	ngFB.init({appId: '308092606198933'});
	
	$ionicPlatform.ready(function() {
	    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
		if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if (window.StatusBar) {
		// org.apache.cordova.statusbar required
			StatusBar.overlaysWebView(false);
			StatusBar.styleDefault();
		}
		if (window.cordova) {
			cordova.getAppVersion.getVersionNumber().then(function (version) {
			   console.log("v = "+version);
			   $localstorage.set("version",version);
			   console.log("device");
			   console.log(device);
			   
			   var platform = device.platform.replace(/(\s*)/g,"");
			   var os_version = device.version.replace(/(\s*)/g,"");
			   console.log(platform);
			   console.log(os_version);
			   if(platform == "Android"){
				   versionChk.google_store().then(function(res){
					   console.log("google");
					   var store_varsion = res.data.substring(res.data.indexOf("softwareVersion\">")+17);
					   store_varsion = store_varsion.substring(0,store_varsion.indexOf("<"));
					   store_varsion = store_varsion.replace(/(\s*)/g,"");
					   console.log(store_varsion);
					   
					   if($localstorage.get('version') != store_varsion){
						   var confirmPopup = $ionicPopup
							.confirm({
								template : '<b class="b1">Bo</b><b class="b2">go</b><b class="b3">ga</b>새버전이 준비되었습니다.<br/> 업데이트를 진행 해주세요.<br/>업데이트가 안뜰 경우 삭제이후 설치해주세요.',
								cssClass : 'logout-popup',
								buttons: [{
								    text: '취소',
								    type: 'button-default',
								    onTap: function(e) {
								    	confirmPopup.close();
								    }
								  }, {
								    text: '확인',
								    type: 'button-positive',
								    onTap: function(e) {
								    	return true;
								    }
								  }]
							});
						confirmPopup.then(function(res) {
							console.log(res);
							if (res) {
								
								window.open("https://play.google.com/store/apps/details?id=com.bogoga"
										, '_system');
							} else {
								console.log('You are not sure');
							}
						});
					   }else{
						   
					   }
				   });
			   }
			   
			   
			   if (os_version >= 6) {
				   var permissions = cordova.plugins.permissions;
				   permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, checkPermissionCallback, null);

				   function checkPermissionCallback(status) {
					   console.log(status);
				     if(!status.hasPermission) {
				       var errorCallback = function() {
				         console.log('Camera permission is not turned on');
				       }

				       permissions.requestPermission(
				         permissions.READ_EXTERNAL_STORAGE,
				         function(status) {
				           if(!status.hasPermission) errorCallback();
				         },
				         errorCallback);
				     }else{
				    	 console.log("sss");
				     }
				   }
			   }
			   
			});
		}
		
	});
	
	
	
	$ionicPlatform.registerBackButtonAction(function (event) {
	    if($state.current.name=="tab.main-list"){
	    	var confirmPopup = $ionicPopup
				.confirm({
					template : '<b class="b1">Bo</b><b class="b2">go</b><b class="b3">ga</b>를 종료하시겠습까?',
					cssClass : 'logout-popup',
					buttons: [{
					    text: '취소',
					    type: 'button-default',
					    onTap: function(e) {
					    	confirmPopup.close();
					    }
					  }, {
					    text: '확인',
					    type: 'button-positive',
					    onTap: function(e) {
					    	return true;
					    }
					  }]
				});
			confirmPopup.then(function(res) {
				console.log(res);
				if (res) {
					navigator.app.exitApp();
				} else {
					console.log('You are not sure');
				}
			});
	      
	    }
	    else {
	      navigator.app.backHistory();
	      $ionicHistory.clearCache();
	    }
	  }, 100);
	
})

.config(function($stateProvider, $urlRouterProvider,$httpProvider,$ionicConfigProvider/*,ionicDatePickerProvider*/) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
	$httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
   
    $ionicConfigProvider.navBar.alignTitle('center');
    /*var datePickerObj = {
    	      inputDate: new Date(),
    	      setLabel: 'Set',
    	      todayLabel: 'Today',
    	      closeLabel: 'Close',
    	      mondayFirst: false,
    	      weeksList: ["워", "월", "화", "수", "목", "금", "토"],
    	      monthsList: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    	      templateType: 'popup',
    	      from: new Date(2012, 8, 1),
    	      to: new Date(2018, 8, 1),
    	      showTodayButton: true,
    	      dateFormat: 'dd MMMM yyyy',
    	      closeOnSelect: false,
    	      disableWeekdays: []
    	    };
	ionicDatePickerProvider.configDatePicker(datePickerObj);*/
    	    
  $stateProvider
  // setup an abstract state for the tabs directive
    .state('tab', {
	    url: '/tab',
	    abstract: true,
	    templateUrl: 'templates/inc/tabs.html',
	    controller: 'TabCtrl'
	})

  // Each tab has its own nav history stack:
/*
  .state('tab.category', {
    url: '/category',
    views: {
      'tab-category': {
        templateUrl: 'templates/category/tab-category.html',
        controller: 'CategoryCtrl'
      }
    }
  })*/
  
  .state('tab.main-list', {
    url: '/main-list/:param',     
    views: {
      'tab-main-list': {   //?? 뭘가 id?
        templateUrl: 'templates/main/main-list.html',   // 리턴되는 view
        controller: 'MainListCtrl'
      }
    }
  })
  
  .state('tab.shop-detail', {
    url: '/shop-detail/:shopId',     
    views: {
      'tab-shopDetail': {   // view name
        templateUrl: 'templates/shop-detail.html',   // 리턴되는 view
        controller: 'ShopDetailCtrl'
      }
    }
  })
  
  .state('tab.search', {
      url: '/search',
      views: {
        'tab-search': {
          templateUrl: 'templates/search/tab-search.html',
          controller: 'SearchCtrl'
        }
      }
    })
    	.state('tab.search_inner', {
		    url: '/search/inner',     
		    views: {
		      'tab-search': {   // view name
		        templateUrl: 'templates/search/search_inner.html',
		        controller: 'SearchInnerCtrl'
		      }
		    }
		  })
  .state('tab.recent', {
    url: '/recent',
    views: {
      'tab-recent': {
        templateUrl: 'templates/tab-recent.html',
        controller: 'RecentCtrl'
      }
    }
  })
  .state('tab.zzims', {
    url : '/zzims',
    views : {
      'tab-zzims' : {
        templateUrl : 'templates/tab-zzims.html',
        controller : 'ZzimsCtrl'
      }
    }
  })
  .state('tab.manager', {
    url : '/manager',
    views : {
      'tab-manager' : {
        templateUrl : 'templates/tab-manager.html',
        controller : 'ManagerCtrl'
      }
    }
  })
  .state('tab.marketJoin', {
    url : '/marketJoin',
    views : {
      'tab-marketJoin' : {
        templateUrl : 'templates/tab-marketJoin.html',
        controller : 'MarketJoinCtrl'
      }
    }
  })

.state('join', {
    url: '/join',
    templateUrl: 'templates/sign/join.html',
    controller: 'JoinCtrl'
})
.state('login', {
    url: '/login',
    templateUrl: 'templates/sign/login.html',
    controller: 'LoginCtrl'
})

.state('setting', {
    url: '/setting',
    templateUrl: 'templates/customer/setting.html',
    controller: 'SettingCtrl'
})
.state('pw_change', {
    url: '/pw_change',
    templateUrl: 'templates/customer/pw_change.html',
    controller: 'PwChangeCtrl'
})
.state('customer_center', {
    url: '/customer_center',
    templateUrl: 'templates/customer/customer_center.html',
    controller: 'CustomerCenterCtrl'
})
.state('accessterms', {
    url: '/accessterms',
    templateUrl: 'templates/customer/accessterms.html',
    controller: 'AccesstermsCtrl'
})
.state('guide', {
    url: '/guide',
    templateUrl: 'templates/customer/guide.html',
    controller: 'GuideCtrl'
})
.state('inquiry', {
    url: '/inquiry',
    templateUrl: 'templates/customer/inquiry.html',
    controller: 'InquiryCtrl'
})
  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/main-list/:param');  //첫화면
  
});
