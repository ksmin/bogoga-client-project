angular.module('starter.services', [])
.factory('NavService',function($http, ApiEndpoint){
	console.log('NavService', ApiEndpoint);

	return {//eventCategory
		 ec_all: function(){
			return $http.get(ApiEndpoint.url +"/service/eventList");
		},//normalCategory
		nc_all : function(){
			return $http.get(ApiEndpoint.url +"/service/categoryList");
		}
	}
})
.factory('userService',function($http, ApiEndpoint){
	
})
.factory('JoinService',function($http, ApiEndpoint){
	console.log('NavService', ApiEndpoint);

	return {
		userSignin: function(dbObj){
			var params = dbObj;
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/signin/userSignin",
			    data: $.param({
			    	email : params.email,
			    	password : params.password
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		emailChk: function(email){
			var params = email;
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/signin/emailChk",
			    data: $.param({
			    	email : params
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		}
	}
})
.factory('CategoryService',function($http, ApiEndpoint){
	console.log('CategoryService', ApiEndpoint);
	var params = {"banner_id":1};
	return {
		all : function(){
			return $http.get(ApiEndpoint.url +"/service/categoryBanner",{ params });
		}
	}
})
.factory('EventCategoryService',function($http, ApiEndpoint){
	console.log('EventCategoryService', ApiEndpoint);
	return {
		all : function(){
			return $http.get(ApiEndpoint.url +"/service/eventList");
		}
	}
})
.factory('EventSearchService',function($http, ApiEndpoint){
	console.log('url', ApiEndpoint);
	return {
		all : function(){
			var params = {
					recommand : 1
			}
			return $http.get(ApiEndpoint.url +"/service/recommandList",{ params });
		}
	}
})
.factory('esService',function($http, ApiEndpoint){
	return {
		local_kw : function(){
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/service/local_kw",
			    data: $.param({}),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		band_kw : function(){
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/service/band_kw",
			    data: $.param({}),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		}
	}
})
.factory('SubwayService',function($http, ApiEndpoint){
	console.log('url', ApiEndpoint);
	return {
		all : function(subwayObj){
			return $http.get(ApiEndpoint.subway 
					+"/"+subwayObj.key
					+"/json/"
					+subwayObj.subline
					+"/1/128/"
					+subwayObj.line_code);
		}
	}
})
.factory('ListCategoryService',function($http, ApiEndpoint){
	console.log('url', ApiEndpoint);
	return {
		all : function(){
			return $http.get(ApiEndpoint.url +"/service/categoryList");
		}
	}
})
.factory('MainList',function($http, ApiEndpoint){
	console.log('url', ApiEndpoint);
	return {
		all : function(dbObj){
			console.log("main list");
			console.log(dbObj);
			var params = dbObj;
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/service/mainList",
			    data: $.param({
			    	email : params.email,
			    	row_s : params.row_s,
			    	row_e : params.row_e,
			    	code_cd : params.code_cd,
			    	station_nm : params.station_nm,
			    	keyword : params.keyword,
			    	order_list : params.order_list
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
			/*return $http.get(ApiEndpoint.loca +dbObj.url,{ params });*/
		}
	}
})
.factory('ShopDetailService',function($http,$stateParams, ApiEndpoint){
	return {
		all : function(shopDetailObj){
			var params = shopDetailObj;
			console.log(params);
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/service/shopDetail",
			    data: $.param({
			    	email : params.email,
			    	shop_id : params.shop_id
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		latlng : function(shop_addr){
			var addr = shop_addr;
			console.log(ApiEndpoint.map+'?sensor=false&language=ko&address='+addr);
			return $http.get(ApiEndpoint.map+'?sensor=false&language=ko&address='+addr
									,{headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}});
		}
	}
})
.factory('PwChangeService',function($http,$localstorage,ApiEndpoint){
	return {
		pwc : function(dbObj){
			var params = dbObj;
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/signin/changePassword",
			    data: $.param({
			    	password : params.password,
			    	email : params.email
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		}
	}
})
.factory('Recent',function($http,$localstorage,ApiEndpoint){
	console.log("Recent service");
	return {
		all : function(dbObj){
			var params = dbObj;
			return $http.get(ApiEndpoint.url +"/service/recentList",{ params });
			return $http({
				method: "POST" ,
			    url: ApiEndpoint.url +"/service/recentList",
			    data: $.param({
			    	shops : params.shops
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		remove: function(shop_id) {
			var tmpList = [];
			var params = {
					shops : ""
			};
			tmpList = $localstorage.get("recentTmp").split(',');
			tmpList.splice(tmpList.indexOf(shop_id),1);
			params.shops = tmpList.toString();
			$localstorage.set("recentTmp",tmpList.toString());
			return $http.get(ApiEndpoint.url +"/service/recentList",{ params });
		}
	}
})
.factory('Zzims',function($http,$localstorage,ApiEndpoint){
	console.log("Zzims service");
	var params = {};
	var user = {
			email : "",
			shop_id : ""
	};

	return {
		all : function(){
			user = JSON.parse($localstorage.get("user"));
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/service/zzimList",
			    data: $.param({
			    	email : user.email
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		remove: function(dbObj) {
			var params = dbObj;
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/service/zzimList_del",
			    data: $.param({
			    	email : params.email,
			    	shop_id : params.shop_id
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		}
	}
})
.factory('MarketJoinService',function($http,$filter, ApiEndpoint){
	console.log('url', ApiEndpoint);
	return {
		post_all : function(dbObj){
			console.log(dbObj);
			return $http.get(ApiEndpoint.url+"/api/postGet?data="+dbObj.data+"&page="+dbObj.page);
		},
		category_all : function(){
			return $http.get(ApiEndpoint.url +"/service/categoryList");
		},
		manager_all : function(){
			return $http.get(ApiEndpoint.url +"/signin/getManager");
		},
		bizrnoChk : function(dbObj){
			var params = dbObj;
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/signin/chkDupBizrno",
			    data: $.param({
			    	biz_r_no : params.biz_r_no
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		marketSignUpdate : function(dbObj){
			var params = dbObj;
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/signin/marketSignUpdate",
			    data: $.param({
			    	email : params.email,
			    	shop_nm : params.shop_nm,
			    	biz_r_no : params.biz_r_no,
			    	code_cd : params.code_cd,
			    	user_nm : params.user_nm,
			    	phone_no : params.phone_no,
			    	u_phone_no : params.u_phone_no,
			    	code_cd : params.code_cd,
			    	station_nm : params.station_nm,
					station_cd : params.station_cd,
					line_num : params.line_num,
					fr_code : params.fr_code,
			    	post_no : params.post_no,
			    	addr : params.addr,
			    	start_dt : $filter('date')(params.start_dt,'yyyyMMdd'),
			    	end_dt : $filter('date')(params.end_dt,'yyyyMMdd'),
			    	mngr_id : params.mngr_id
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		imgUpload : function(dbObj){
			var params = dbObj;
			console.log("service");
			//앵귤러의 경우 post로 데이터를 보낼시 직렬화를 해주어야한다
			//$.param <<
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/service/imgUpload",
			    data: $.param({
			    	email : params.email,
			    	en_base64 : params.en_base64,
			    	size : params.size,
			    	ext : params.ext,
			    	code : params.code,
			    	reg_date : params.reg_date,
			    	file_seq : params.file_seq,
			    	view_position : params.view_position,
			    	view_chk : params.view_chk
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
	}
})
.factory('ManagerService',function($http,ApiEndpoint){
	return {
		all : function(dbObj){
			var params = dbObj.params;
			return $http.get(ApiEndpoint.url +dbObj.url,{ params });
		},
		post_all : function(dbObj){
			console.log(dbObj);
			return $http.get(ApiEndpoint.url+"/api/postGet?data="+dbObj.data+"&page="+dbObj.page);
		},
		subway_all : function(dbObj){
			return $http.get(ApiEndpoint.subway 
					+"/"+dbObj.key
					+"/json/"
					+dbObj.subline
					+"/1/128/"
					+dbObj.line_code);
		},
		category_all : function(){
			return $http.get(ApiEndpoint.url +"/service/categoryList");
		},
		modify : function(dbObj){
			var params = dbObj;
			console.log("???????");
			console.log(params);
			return $http.get(ApiEndpoint.url +"/service/shopModify",{params});
		},
		imgUpload : function(dbObj){
			var params = dbObj;
			console.log("service");
			//앵귤러의 경우 post로 데이터를 보낼시 직렬화를 해주어야한다
			//$.param <<
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/service/imgUpload",
			    data: $.param({
			    	shop_id : params.shop_id,
			    	en_base64 : params.en_base64,
			    	size : params.size,
			    	ext : params.ext,
			    	code : params.code,
			    	reg_date : params.reg_date,
			    	file_seq : params.file_seq,
			    	view_chk : params.view_chk,
			    	view_position : params.view_position,
			    	upload_type : params.upload_type
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		},
		imgDelete : function(dbObj){
			var params = dbObj;
			return $http.get(ApiEndpoint.url +"/service/imgDelete",{params});
		},bannerList : function(dbObj){
			var params = dbObj;
			return $http({
			    method: "POST" ,
			    url: ApiEndpoint.url +"/service/bannerList",
			    data: $.param({
			    	shop_id : params.shop_id
			    }),
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			    }
			});
		}
	}
})

;
