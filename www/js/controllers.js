angular.module('starter.controllers', ['starter.services','starter.collection', 'ngOpenFB'])
.controller('TabCtrl', function($scope,$state, $ionicModal,$ionicPopover,$localstorage,$ionicHistory,$ionicLoading,$timeout,$ionicPopup,toaster,LoginChk,esService) {
	
	$ionicPopover.fromTemplateUrl('templates/user-menu.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(popover) {
		$scope.popover = popover;
	});

	$scope.goUserMenu = function($event){
		$scope.popover.show($event);
		
		$scope.login_bool = $localstorage.ask('user'); //로그인체크
		if($scope.login_bool){  //첫 한번만 실행
			$scope.user = JSON.parse($localstorage.get("user"));
			login_common();
			
			//사업자 가입 했는지 여부 체크
			console.log("user");
			console.log($scope.user);
			LoginChk.biz_chk($scope.user.email).then(function(res){
				console.log("biz_chk");
				console.log(res);
				
				userInfoFn(res);
			});
			
		}else{
			console.log("none");
		}
		
		esService.band_kw().then(function(res){
			console.log(res.data);
			var band_link = new Array();
			for(var i in res.data){
				var obj = {
						tit : "",
						link : ""
				};
				if(res.data[i].type=='TM'
					|| res.data[i].type=='BR'
						|| res.data[i].type=='FM'){
					switch(res.data[i].type){
						case 'TM' :
							obj.tit = '오늘의 메뉴 보러가기!';
							obj.link = res.data[i].band_kw
							break;
						case 'BR' :
							obj.tit = '보고가 추천 할인!!!';
							obj.link = res.data[i].band_kw
							break;
						case 'FM' :
							obj.tit = '인기 동영상 보러가기!';
							obj.link = res.data[i].band_kw
							break;
					};
					band_link.push(obj);
				}
			}
			$scope.band_link = band_link;
		});
		
		$scope.bandLinkAct = function(item){
			console.log(item.link);
			if(item.link){
				var link = (String(item.link).startsWith('http://') 
					|| String(item.link).startsWith('https://'))
				? item.link : 'http://'+item.link;
		        window.open(link, '_blank', 'transitionstyle=crossdissolve,toolbarposition=top');
			}else{
				toaster.pop('error'
						, "알림창"
						, "등록된 정보가 없습니다."
						, 2000);
			}
			return false;
		}
	};
	
	function userInfoFn(res){
		var bool = res.data[0] ? true : false;
		$scope.thumb = "img/icon/thumb"+Math.floor((Math.random()*4)+1)+".png";
		if(bool){
			$scope.biz_chk = true;
			var userTmp = JSON.parse($localstorage.get("user"));
			userTmp.shop_id  = res.data[0].shop_id;
			userTmp.shop_nm = res.data[0].shop_nm;
			userTmp.tn = res.data[0].shop_img;
			$localstorage.set("user",JSON.stringify(userTmp));
			$scope.user = userTmp;
			if(userTmp.shop_img!=null){
				$scope.thumb = "http://52.79.78.117/img/"+tmpVal.shop_img;
			}
		}else{
			$scope.biz_chk = false;
			switch($scope.user.login_type){
				case 'normal' :
					 break;
				case 'facebook' : 
					$scope.thumb = "http://graph.facebook.com/"+$scope.user.id+"/picture?width=70&height=70";
					break;
				case 'kakao' : 
					break;
			};		
		}
	}
	
	
	$scope.goLogin = loginFn;
	$scope.manager = managerFn;
	$scope.marketJoin = marketJoinFn;
	$scope.customer = customerFn;
	
	
	
	function login_common(){
		var tmpVal=JSON.parse($localstorage.get("user"));
		switch(tmpVal.login_type){
			case 'normal' :
				$scope.thumb = "http://52.79.78.117/img/"+tmpVal.shop_img;
				if(tmpVal.shop_img==null){
					$scope.thumb = "img/icon/thumb"+Math.floor((Math.random()*4)+1)+".png";
				}
				 break;
			case 'facebook' : 
				$scope.thumb = "http://graph.facebook.com/"+tmpVal.id+"/picture?width=70&height=70";
				break;
			case 'kakao' : 
				break;
		};		
	};
	
	
	function loginFn(){
		menuClose();
		$state.go("login");
	}	
	function managerFn(){
		menuClose();
		$state.go("tab.manager");
    }
	function marketJoinFn(){
		menuClose();
		$state.go("tab.marketJoin");
	}
	function customerFn(kind){
		menuClose();
		switch(kind){
			case 'setting' : $state.go("setting");
				break;
				
			case 'customer_center' :$state.go("customer_center");
				break;
		};
	};
	
	$scope.band_link = function(){
		
	}
	
	function menuClose(){
		$scope.popover.hide();
	}

})
.controller('LoginCtrl',function($scope,$state,ngFB,$ionicPopup,$localstorage,$ionicHistory,$ionicLoading,$timeout,_Utile,toaster,LoginChk){

	$scope.fbLogin = fbLogin;
	$scope.defaultLogin = defaultLogin;
	$scope.join = joinFn;
	
	$scope.data = {
			user_email : "",
			user_pw : ""
	};
	
	
	
	//일반 로그인
	function defaultLogin(){
		$scope.loading = //로딩화면 생성
		    $ionicLoading.show({
		      content: 'Loading',
		      animation: 'fade-in',
		      showBackdrop: true,
		      maxWidth: 200,
		      showDelay: 0
		});
		
		LoginChk.get($scope.data).then(function(res){
			console.log("LoginChk111");
			console.log(res);
			if(res.data.length > 0 && res.data[0].user_id !=null){
				res.data[0].login_type = "normal";
				$localstorage.set("user",JSON.stringify(res.data[0]));
				$scope.user = res.data[0];
				$timeout(function () {
	    		    $ionicLoading.hide();
	    		    toaster.pop("success"
            				, "알림창"
            				, "로그인 성공"
            				, 2000);
	    		    login_common();
				});
				
			}else{
				$ionicLoading.hide();
				toaster.pop("error"
        				, "알림창"
        				, "로그인 실패"
        				, 2000);
			}
		});
	};

	//페이스북 로그인	
	function fbLogin () {
	    ngFB.login({scope: 'email,publish_actions'}).then(
	        function (response) {
	            if (response.status === 'connected') {
	                console.log('Facebook login succeeded');
	                ngFB.api({
	                    path: '/me'
	                }).then(
	                    function (user) {
	                    	console.log(user);
	                    	var msg = "";
	                    	var type = "success";
	                    	var bool = true;
	                    	
	                    	if(user.email != null){
	                    		msg = "로그인 성공";
	                    	}else{
	                    		type = "error";
	                    		bool = false;	                    			
	                    		msg = "해당 페이스북 계정에 email 정보가 없습니다." +
	                    				"다른 계정이나 해당 계정에 email 정보를 추가하신 후" +
	                    				"로그인 해주시기 바랍니다.";
	                    	}
	                    	
	                    	toaster.pop(type
	                				, "알림창"
	                				, msg
	                				, 2000);
							if(bool){
								user.login_type = "facebook";
								$localstorage.set("user",JSON.stringify(user));
								$scope.user = user;
								login_common();
							}
	                    },
	                    function (error) {
							toaster.pop('error'
	                				, "알림창"
	                				, "error : "+error.error_description
	                				, 2000);
	                    });
	            } else {
					toaster.pop('error'
            				, "알림창"
            				, "Facebook login failed"
            				, 2000);
	            }
	        });
	};
	function login_common(){
		var tmpVal=JSON.parse($localstorage.get("user"));
		switch(tmpVal.login_type){
			case 'normal' :
				$scope.thumb = "http://52.79.78.117/img/"+tmpVal.shop_img;
				 break;
			case 'facebook' : 
				$scope.thumb = "http://graph.facebook.com/"+tmpVal.id+"/picture?width=70&height=70";
				break;
			case 'kakao' : 
				break;
		};
		
		/*사업자 체크해서 개인일 경우 사업자 유도 팝업 실행*/
		LoginChk.biz_chk(tmpVal.email).then(function(res){
			console.log("email = "+tmpVal.email);
			console.log("biz_chk");
			console.log($scope.user);
			if(!res.data[0]){
				var marketGuide_popup = $ionicPopup
						.show({
							templateUrl: 'templates/popup/marketGuide.html',
							cssClass: 'marketGuide-popup',
							scope : $scope
						});
				marketGuide_popup.then(function(res) {
					console.log('Tapped!', res);
				});
				
				$scope.popup_close = function(){
					marketGuide_popup.close();
				};
				$scope.goMarketJoin = function(){
					marketGuide_popup.close();
					$state.go("tab.marketJoin");
				}
			}else{
				
			}
		});
		
		$state.go('tab.main-list',{param:""});
	};
	
	function joinFn(){
		$state.go("join");
	};
	
	$scope.back = function (){
		_Utile.back();
	}
})
.controller('JoinCtrl', function($scope,$state, $ionicModal,$ionicPopup,$ionicLoading,$timeout,_Utile,toaster,Assent,JoinService) {
	console.log("JoinCtrl");
	$scope.back = function(){
		_Utile.back();
	};
	
	$scope.data = {
			email : "",
			email_chk : false,
			password : "",
			password_chk : "",
			access : false,
			personal : false
	};
	
	$scope.assentPop = function(kind){
		Assent.action($scope,kind);
	};
	
	$scope.emailChk = function(){
		$scope.data.email = $scope.data.email.replace( /(\s*)/g, "");
		console.log($scope.data.email);
		JoinService.emailChk($scope.data.email).then(function(res){
			console.log("emil chk");
			console.log(res.data[0]);
			if(res.data[0] != null){
				toaster.pop("error"
						, "알림창"
						, "이미 있는 이메일 입니다."
						, 2000);
				$scope.data.email_chk = false;
			}else{
				toaster.pop("success"
						, "알림창"
						, "사용 가능 한 이메일 입니다."
						, 2000);
				$scope.data.email_chk = true;
			}
			
		});
	};
	
	$scope.join_submit = function(){
		console.log($scope.data);
		if(joinChk()){
			JoinService.userSignin($scope.data).then(function(res){
				console.log("회원가입 여부");
				console.log(res.data[0]);
				$state.go("login");
			});
		}
	};
	
	
	function joinChk(){
		var data = $scope.data;
		var type = "success",
			msg = "회원가입 완료",
			bool = true;
		
		if(data.email == ''){
			type = "error";
			msg = "이메일을 입력 해주세요";
			bool = false;
		}else if(!data.email_chk){
			type = "error";
			msg = "이메일 중복 체크를 해주세요.";
			bool = false;
		}else if(data.password == ''){
			type = "error";
			msg = "비밀번호를 입력해주세요";
			bool = false;
		}else if(data.password != data.password_chk){
			type = "error";
			msg = "비밀번호 재입력이 잘못 되었습니다";
			bool = false;
		}else if(!data.personal){
			type = "error";
			msg = "개인정보 취급방침 동의를 체크 해주세요";
			bool = false;
		}else if(!data.access){
			type = "error";
			msg = "서비스 이용약관 동의를 체크 해주세요";
			bool = false;
		};
		toaster.pop(type
				, "알림창"
				, msg
				, 2000);
		return bool;
	};
	
	
})
.controller('MarketJoinCtrl', function($scope,$filter, $ionicModal,$state,$localstorage, $cordovaImagePicker, $ionicPlatform,$ionicPopup,$ionicLoading,$timeout,$ionicHistory,toaster,MarketJoinService,Subway,Post,_Utile,CategoryPop,Assent) {
	console.log("MarketJoinCtrl");
	
	$scope.login_bool = $localstorage.ask('user');
	if($scope.login_bool){  //첫 한번만 실행
		$scope.user = JSON.parse($localstorage.get("user"));
	}else{
		console.log("login fail");
	};
	
	$scope.data = {
			shop_nm : "",  //매장이름
			biz_r_no : "",  //사업자번호
			biz_r_no_chk : false,  //사업자번호 중복 체크
			user_nm : "",  //사업장 주인명
			phone_no : "", //전화번호
			u_phone_no : "", //개인 휴대폰
			post_no : "", //우편번호
			addr : "",  //상세주소
			mngr_id : "", //관리자 
			start_dt : "", // 시작날자
			end_dt : "", //종료날자
			code_cd : "", //카테고리 코드
			station_nm : "",  //역명
			station_cd : "",  //역코드
			line_num : "",  //호선
			fr_code : "",	//
			access : false, //서비스이용약관 체크항목
			personal : false//개인정보취급방침 체크항목
	};

	$scope.category_pop = function(){
		CategoryPop.action($scope);
	}
	
	
	$scope.startMin = new Date();
	$scope.endMin = new Date();
	//날짜
	$scope.startDateFn = function (val) {		
		if (!val) {	
	        console.log('Date not selected');
	    } else {
	    	$scope.data.start_dt = val;
	    	$scope.endMin = val;
	        console.log('Selected date is : ', $filter('date')(val,'yyyyMMdd'));
	    }
	};
	$scope.endDateFn = function (val) {
		if (!val) {	
	        console.log('Date not selected');
	    } else {
	    	$scope.data.end_dt =val;
	        console.log('Selected date is : ', val);
	    }
	};


	//우편
	$scope.postAction = function(){
		Post.action($scope);
	};
	//사업자번호 체크
	$scope.biz_r_noChk = function(){
		console.log($scope.data.biz_r_no);
		if(!$scope.data.biz_r_no){
			toaster.pop('error'
    				, "알림창"
    				, "사업자 번호가 잘못 되었습니다."
    				, 2000);
		}else{
			var bizrnoChkObj = {
					biz_r_no : $scope.data.biz_r_no
			};
			MarketJoinService.bizrnoChk(bizrnoChkObj).success(function(res){
				console.log("bizrnoChk");
				console.log(res);
				if(res){
					toaster.pop('error'
	        				, "알림창"
	        				, "사업자 번호가 이미 있습니다."
	        				, 2000);
					$scope.data.biz_r_no = null;
					$scope.data.biz_r_no_chk = false;
				}else{
					toaster.pop('success'
	        				, "알림창"
	        				, "사용 가능한 사업자 번호 입니다."
	        				, 2000);
					$scope.data.biz_r_no_chk = true;
				}
			});
		};
	};
	//사업자 등록증 등록
	$scope.biz_r_imgReg = function(){
		/*
		$scope.loading = //로딩화면 생성
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
		*/
		var options = {
	    		maximumImagesCount: 1,
	    		quality: 80,
	    		destinationType : Camera.DestinationType.DATA_URL,
	    		sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
	    		width : 680,
	    		allowEdit: true,
	    		encodingType: Camera.EncodingType.JPEG,
	    		popoverOptions: CameraPopoverOptions,
	    		saveToPhotoAlbum: false
	    };
	    $cordovaImagePicker.getPictures(options)
	    	.then(function (results) {

	    		// 안드로이드에서 Cancel 버튼을 눌렀을 경우 결과.
	    		if(results.length === 0) {
	    			return false;
	    		}

	    		var imgArr = new Array();
	    		var imgSize = new Array();
	    		var imgData = {};
	    		
	    		$scope.loading = //로딩화면 생성
					$ionicLoading.show({
						content: 'Loading',
						animation: 'fade-in',
						showBackdrop: true,
						maxWidth: 200,
						showDelay: 0
					});

	    		window.plugins.Base64.encodeFile(results[0], function(base64){
                    // Save images in Base64
                    var base64Tmp = base64.substring(base64.indexOf(',')+1);
                    var imgSize = parseInt((base64Tmp.length/1333)*1000);
                    imgData = {
                    		src : "data:image/jpeg;base64,",
	    					en_base64 : base64Tmp,
	    					file_seq : 0,  //사업자등록증 seq는 0 고정
	    					view_position : 0,
	    					view_chk : false,
	    					code : 'BZ',
	    					ext : 'JPEG',
	    					size : imgSize,
	    					reg_date : $filter('date')(new Date(),'yyyyMMddHHmmss')
	    				};
                 });
	    		$timeout(function () {
	    		    $ionicLoading.hide();
	    		    $scope.biz_img = imgData;
	    		    console.log(imgData);
	    		}, 2000);
	    }, function(error) {
	        console.log('Error: ' + JSON.stringify(error));    // In case of error
	    });
	};
	//담당자
	MarketJoinService.manager_all().success(function(res){
		console.log("manager_all");
		console.log(res);

		$scope.manager_list = res;
		$scope.manager_item = {
				mngr_id : res[0].mngr_id
		};
		$scope.data.mngr_id = res[0].mngr_id;
		
		$scope.managerAction = function(item){
			console.log(item);
			console.log(item.mngr_id);
			$scope.data.mngr_id  = item.mngr_id;
		}
	});
	//지하철
	$scope.subwayAction = function(){
		Subway.action($scope);
	};
	
	$scope.assentPop = function(kind){
		Assent.action($scope,kind);
	}
	
	
	//전송
	$scope.market_submit = function(){
		console.log("submit");
		console.log($scope.data);
		$scope.data.email = $scope.user.email;
		if(signChk()){
			
			$scope.data.email = $scope.user.email;
			$scope.loading = //로딩화면 생성
				$ionicLoading.show({
					content: 'Loading',
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 0
				});
			MarketJoinService.marketSignUpdate($scope.data).then(function(res){
				console.log("가입 성공 결과값");
				console.log(res);
				if(res.data[0]!=null && res.data[0].result == "Success"){  //이미지
					console.log("Success");
					var imgParam = new Object();
					if($scope.biz_img){
						imgParam = $scope.biz_img;
						imgParam['email'] = $scope.data.email;
						console.log(imgParam);
						MarketJoinService.imgUpload(imgParam).success(function(res){
							console.log("img upload success");
							console.log(res);
							$timeout(function(){
								$ionicLoading.hide();
								var stateParams = $ionicHistory.backView().stateParams?$ionicHistory.backView().stateParams.param:"";
								$state.go($ionicHistory.backView().stateName,{param:stateParams});
								$ionicHistory.clearCache();
							}, 2000);
						});
					}else{
						$timeout(function(){
							$ionicLoading.hide();
							var stateParams = $ionicHistory.backView().stateParams?$ionicHistory.backView().stateParams.param:"";
							$state.go($ionicHistory.backView().stateName,{param:stateParams});
							$ionicHistory.clearCache();
						}, 2000);
					}
					
					
				}else{
					$ionicLoading.hide();
					toaster.pop("error"
							, "알림창"
							, "서버 에러"
							, 2000);
					console.log("?????????");
				};
			});
		};
	};
	
	
	function signChk(){
		var data = $scope.data;
		var type = "success";
		var msg = "승인요청을 보냅니다.";
		var bool = true;

		if(data.shop_nm == ""){
			type = "error";
			msg = "매장 이름을 적어주세요";
			bool = false;
		}else if(data.biz_r_no == ""){
			type = "error";
			msg = "사업자 번호를 적어주세요";
			bool = false;
		}else if(!data.biz_r_no_chk){
			type = "error";
			msg = "사업자 번호 중복 체크 해주세요";
			bool = false;
		}else if($scope.biz_img == null){
			type = "error";
			msg = "사업자 이미지를 넣어주세요";
			bool = false;
		}else if(data.code_cd == ""){
			type = "error";
			msg = "카테고리를 지정해 주세요";
			bool = false;
		}else if(data.station_nm == ""){
			type = "error";
			msg = "지하철을 지정해 주세요";
			bool = false;
		}else if(data.user_nm == ""){
			type = "error";
			msg = "대표자 이름을 적어주세요";
			bool = false;
		}else if(data.phone_no == ""){
			type = "error";
			msg = "전화번호를 적어주세요";
			bool = false;
		}else if(data.u_phone_no == ""){
			type = "error";
			msg = "핸드폰 번호를 적어주세요";
			bool = false;
		}else if(data.post_no == ""){
			type = "error";
			msg = "주소를 입력해주세요";
			bool = false;
		}else if(data.addr == ""){
			type = "error";
			msg = "주소를 입력해주세요";
			bool = false;
		}else if(data.start_dt == ""){
			type = "error";
			msg = "서비스 시작일을 지정해 주세요";
			bool = false;
		}else if(data.end_dt == ""){
			type = "error";
			msg = "서비스 종료일을 지정해 주세요";
			bool = false;
		}else if(data.mngr_id == ""){
			type = "error";
			msg = "담당자를 지정해 주세요";
			bool = false;
		}else if(!data.personal){
			type = "error";
			msg = "개인정보 취급방침 동의를 체크 해주세요";
			bool = false;
		}else if(!data.access){
			type = "error";
			msg = "서비스 이용약관 동의를 체크 해주세요";
			bool = false;
		};
		
		toaster.pop(type
				, "알림창"
				, msg
				, 2000);
		
		return bool;
	};
	
	$scope.back = function(){
		_Utile.back();
	}
	
})
.controller('ManagerCtrl', function($scope,$filter,$state,$localstorage, $cordovaImagePicker, $ionicPlatform,$ionicPopup,$ionicLoading,$timeout,$ionicHistory,CallService,ManagerService,_Utile,Subway,Post) {
	console.log("ManagerCtrl");

	////////////////////////////////
	var user = JSON.parse($localstorage.get("user"));
	console.log(user);
	var allObj = {
			url : "/service/shopManage",
			params : {
				shop_id : user.shop_id
			}
	};
	ManagerService.all(allObj).then(function(res){
		console.log("manager");	
		console.log(res.data[0]);	
		$scope.data = res.data[0];
		
		var bannerListObj = {
				shop_id : user.shop_id
		}
		ManagerService.bannerList(bannerListObj).success(function(res){
			console.log("banner list");
			console.log(res);
			if(res){
				for(var i in res){
					res[i].src = "http://52.79.78.117/img/";
					res[i].en_base64 = res[i].shop_img
				}
				$scope.shop_img = res;
			}
		});
				
		console.log(res.data[0]);
		
		if(res.data[0].keyword != null){
			$scope.data.keyword = _Utile.kwChange(res.data[0].keyword);
		}
		
		$scope.local_num = CallService.local;
		$scope.cell_num = CallService.cell;
		
		$scope.subway_no = "호선";
		$scope.subway_nm = "역명";
		
		ManagerService.category_all().then(function(res){
			console.log("category_list");
			console.log(res.data);

			$scope.category_list = res.data;
			
		});
		$scope.category_pop = function(){
			var categoryPopup = $ionicPopup.show({
				templateUrl: 'templates/popup/category_list.html',
			    title: '카테고리선택',
			    cssClass: 'category-popup',
			    scope: $scope
			});
			
			$scope.popup_close = function(){
				categoryPopup.close();
			};
			
			$scope.categoryAction = function(item){
				console.log(item);
				$scope.data.code_cd = item.code_cd;
				$scope.data.code_nm = item.code_nm;
				categoryPopup.close();
			}
		};
		
		
		
		//우편
		$scope.postAction = function(){
			console.log("post??????");
			Post.action($scope);
		};
		//지하철
		$scope.subwayAction = function(){
			Subway.action($scope);
		};
		//////////subway end
		

		//////////////이미지 새로
		$scope.getImageSaveContact = function() {       
			/*
			$scope.loading = //로딩화면 생성
				$ionicLoading.show({
					content: 'Loading',
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 0
				});
			*/
			var maxImg = 7;
		    var options = {
		    		maximumImagesCount: maxImg,
		    		quality: 80,
		    		destinationType : Camera.DestinationType.DATA_URL,
		    		sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
		    		width : 680,
		    		allowEdit: true,
		    		encodingType: Camera.EncodingType.JPEG,
		    		popoverOptions: CameraPopoverOptions,
		    		saveToPhotoAlbum: false
		    };
		    
		    $cordovaImagePicker.getPictures(options)
		    	.then(function (results) {
		    		// 안드로이드에서 Cancel 버튼을 눌렀을 경우 결과.
		    		if(results.length === 0) {
		    			return false;
		    		}

		    		var imgArr = new Array();
		    		var imgSize = new Array();
		    		$scope.imgUploadType = "T";
		    		
		    		$scope.loading = //로딩화면 생성
						$ionicLoading.show({
							content: 'Loading',
							animation: 'fade-in',
							showBackdrop: true,
							maxWidth: 200,
							showDelay: 0
						});

		    		for(var i=0;i<results.length;i++){
		    			
		    			window.plugins.Base64.encodeFile(results[i], function(base64){
		                    // Save images in Base64
		                    var base64Tmp = base64.substring(base64.indexOf(',')+1);
		                    var imgSize = parseInt((base64Tmp.length/1333)*1000);
		                    var imgData = {
		                    		src : "data:image/jpeg;base64,",
			    					en_base64 : base64Tmp,
			    					view_position : 0,
			    					code : 'BB',
			    					ext : 'JPEG',
			    					size : imgSize,
			    					view_chk : true,
			    					reg_date : $filter('date')(new Date(),'yyyyMMddHHmmss'),
			    					upload_type : 'T' //전체 재 등록 이기 때문에
			    				};
			    				
			    				imgArr.push(imgData);
		                 });
		    		}
		    		$timeout(function () {
		    		    $ionicLoading.hide();
		    		    //$scope.img_list = imgArr;
		    		    console.log("imgarr size"+imgArr.length);
		    		    if(imgArr.length > 0){
		    		    	$scope.shop_img = imgArr; //기존 이미지 비움
		    		    }
		    		    
		    		}, 2000);
		    		         
		    		
		    }, function(error) {
		        console.log('Error: ' + JSON.stringify(error));    // In case of error
		    });
		}; 
		/////////// 이미지 end
		
		
		
		//전송
		$scope.manager_submit = function(){
			
			$scope.loading = //로딩화면 생성
				$ionicLoading.show({
					content: 'Loading',
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 0
				});
			
			console.log("manager_submit");
			console.log($scope.data);
			
			function kwChk(){
				var bool = false;
				var special_pattern = /[`~!@$%^&*|\\\'\";:\/?]/gi;
				if($scope.data.keyword !=null){
					if( special_pattern.test($scope.data.keyword) == true){
					    alert('#을 제외한 특수문자는 사용할 수 없습니다.');
					    bool = true;
					}else if($scope.data.keyword.indexOf("#") > -1 == false){
						 alert('키워드 구분에 #은 필수 입니다.');
						 bool = true;
					}
				}
			};

			if(kwChk()){
				return;
			};

			var modifyObj = {
					email : user.email, 
					shop_id : user.shop_id,
					shop_nm : $scope.data.shop_nm,
					phone_no : $scope.data.phone_no,
					cell_no : $scope.data.cell_no,
					call_choice : $scope.data.call_choice,
					post_no : $scope.data.post_no,
					addr : $scope.data.addr,
					dscnt_txt : $scope.data.dscnt_txt,
					code_cd : $scope.data.code_cd,
					wifi_yn : $scope.data.wifi_yn?1:0,
					park_yn : $scope.data.park_yn?1:0,
					deliv_yn : $scope.data.deliv_yn?1:0,
					wrap_yn : $scope.data.wrap_yn?1:0,
					takbe_yn : $scope.data.takbe_yn?1:0,
					toilet_yn : $scope.data.toilet_yn?1:0,
					//web_link : $scope.data.web_link,
					keyword : $scope.data.keyword,
					contents : $scope.data.contents,
					station_nm : $scope.data.station_nm,
					station_cd : $scope.data.station_cd,
					line_num : $scope.data.line_num,
					fr_code : $scope.data.fr_code
			};
			
			console.log(modifyObj);
			

			ManagerService.modify(modifyObj).success(function(res){
				console.log("modify");
				console.log(res);
				if(res[0].result == "Success"){  //이미지
					console.log($scope.imgUploadType);
					if($scope.imgUploadType == "T"){
						var imgDelete = {
								upload_type : "T",
								code : "BB",
								shop_id : user.shop_id
						};
						ManagerService.imgDelete(imgDelete).success(function(res){
							console.log("img upload success");
							console.log($scope.shop_img);
							for(var i=0;i<$scope.shop_img.length;i++){
								var imgParam = new Object();
								imgParam = $scope.shop_img[i];
								imgParam['shop_id'] = user.shop_id;
								imgParam['file_seq'] = i+1;
								imgParam['view_position'] = i;
								
								ManagerService.imgUpload(imgParam).success(function(res){
									console.log("img upload success");
									console.log(res);
								});
								$timeout(function(){}, 2000);
							};
						});
					}else if($scope.imgUploadType == "S"){
						console.log("s");
						console.log($scope.shop_img);
						
						for(var i=0;i<$scope.shop_img.length;i++){
							var imgParam = new Object();
							imgParam = $scope.shop_img[i];
							console.log(imgParam);
							ManagerService.imgUpload(imgParam).success(function(res){
								console.log("img upload success");
								console.log(res);
							});
							$timeout(function(){}, 2000);
						};
					}else{ //체크만 변경할 경우
						console.log("NONE");
						console.log($scope.shop_img);
						for(var i=0;i<$scope.shop_img.length;i++){
							var imgParam = new Object();
							imgParam = $scope.shop_img[i];
							imgParam['shop_id'] = user.shop_id;
							imgParam['code'] = 'BB';
							imgParam['size'] = 0;
							imgParam['ext'] = "";
							imgParam['view_position'] = 0;
							imgParam['upload_type'] = 'N';
							console.log("chk");
							console.log(imgParam);
							ManagerService.imgUpload(imgParam).success(function(res){
								console.log("img upload success");
								console.log(res);
							});
							$timeout(function(){}, 2000);
						};

					}
					$ionicLoading.hide();
					var stateParams = $ionicHistory.backView().stateParams?$ionicHistory.backView().stateParams.param:"";
					console.log("back");
					console.log($ionicHistory.backView().stateParams);
					$state.go($ionicHistory.backView().stateName,{param:stateParams});
					$ionicHistory.clearCache();
				}else{
					console.log("?????????");
				};
			});  		
		};
		
		
		$scope.imgReg = function(){
			var imgRegPopup = $ionicPopup.show({
				templateUrl: 'templates/popup/imgReg.html',
			    title: '이미지',
			    cssClass: 'imgReg-popup',
			    scope: $scope
			});
			
			$scope.popup_close = function(){
				imgRegPopup.close();
			}
		};
		
		
	});
	///////////////////////////////

	$scope.imgScale = function(item){
		var subwayPopup = $ionicPopup.show({
			templateUrl: 'templates/popup/imgScale.html',
		    title: '이미지',
		    cssClass: 'imgScale-popup',
		    scope: $scope,
		    buttons: [
		              { text: 'Cancel' }
		            ]
		});
		$scope.scale_psth = item;
		console.log(item);
		
	};
	$scope.imgChange = function(_index){
		console.log(_index);
		var maxImg = 1;
	    var options = {
	    		maximumImagesCount: maxImg,
	    		quality: 80,
	    		destinationType : Camera.DestinationType.DATA_URL,
	    		sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
	    		width : 680,
	    		allowEdit: true,
	    		encodingType: Camera.EncodingType.JPEG,
	    		popoverOptions: CameraPopoverOptions,
	    		saveToPhotoAlbum: false
	    };
	    
	    $cordovaImagePicker.getPictures(options)
	    	.then(function (results) {
    			// 안드로이드에서 Cancel 버튼을 눌렀을 경우 결과.
	    		if(results.length === 0) {
	    			return false;
	    		}

	    		var imgObj = new Array();
	    		$scope.imgUploadType = "S";
	    		
	    		$scope.loading = //로딩화면 생성
					$ionicLoading.show({
						content: 'Loading',
						animation: 'fade-in',
						showBackdrop: true,
						maxWidth: 200,
						showDelay: 0
					});

	    		window.plugins.Base64.encodeFile(results[0], function(base64){
                    // Save images in Base64
                    var base64Tmp = base64.substring(base64.indexOf(',')+1);
                    var imgSize = parseInt((base64Tmp.length/1333)*1000);
                    imgObj = {
                    		src : "data:image/jpeg;base64,",
	    					en_base64 : base64Tmp,
	    					file_seq : (_index+1),
	    					view_position : _index,
	    					code : 'BB',
	    					ext : 'JPEG',
	    					size : imgSize,
	    					view_chk : true,
	    					reg_date : $filter('date')(new Date(),'yyyyMMddHHmmss'),
	    					shop_id : user.shop_id,
	    					upload_type : 'S' // 개별등록
	    				};
                });
	    		$timeout(function () {
	    		    $ionicLoading.hide();
	    		    console.log("imgarr index - "+_index);
	    		    $scope.shop_img[_index] = imgObj; //기존 이미지 변경
	    		    
	    		}, 2000);
	    		         
	    		
	    }, function(error) {
	        console.log('Error: ' + JSON.stringify(error));    // In case of error
	    });
	}
	
	function convertImgToBase64URL(url, callback, outputFormat){ //base64 인코딩

        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function(){
        	var dataURL;
            canvas.height = img.height;
            canvas.width = img.width;
            ctx.drawImage(img, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            callback(dataURL);
            canvas = null; 
	    };
        img.src = url;
    };
	
	
	$scope.back = function(){
		_Utile.back();
	}
	
})

.controller('CategoryCtrl', function($scope,$state,$localstorage,CategoryService) {
	var ele = document.querySelector('.category');
	$scope.attr ={
     	   deviceH : window.screen.height,
    	   viewH : ele.offsetHeight+50
	};
	
	CategoryService.all().then(function(res){
		$scope.bannerList = res.data;
	});


  // Called each time the slide changes
	$scope.slideChanged = function(index) {
		$scope.slideIndex = index;
		//$ionicSlideBoxDelegate.update(); 
	};
	  
	 /*카테고리 탭 영역 */
	  var default_tab_path = 'templates/category/inc/';
	  $scope.tabList = [
	                    {tabTitle : '이색할인',active:true,url:'event-category.html'},
	                    {tabTitle : '카테고리',active:false,url:'list-category.html'}
	                    ];
	  $scope.tab = {url: 'event-category.html'};
	  $scope.tab.url = default_tab_path+$scope.tab.url;
	  
	  $scope.category_tab = function(item){
		  console.log(item);
		  resetActive();

		  if(!item.active){
			  $scope.tab.url = default_tab_path+item.url;
			  item.active = !item.acvite;
		  }
		  
	  }
	  
	  function resetActive(){
		  for(var i in $scope.tabList){
			  $scope.tabList[i].active = false;
		  }
	  }
	  /* end 카테고리 탭 영역 */
	  
	  $scope.goMain = function(){
		  var arg = arguments;
		  var path = "tab.main-list";
		  console.log(arg.length);
		  switch(arg.length){
			  case 1 :
				  break;
			  case 2 :
				  break;
			  default:
		  }
		  //console.log(arg[0]);
		  $state.go(path);
	  }

})
.controller('EventCategoryCtrl', function($scope,$state,EventCategoryService) {
	EventCategoryService.all().then(function(res){
		console.log(res);
		$scope.eventList = res.data;
	});
})
.controller('ListCategoryCtrl', function($scope,$state,ListCategoryService) {
	ListCategoryService.all().then(function(res){
		console.log(res);
		$scope.categoryList = res.data;
	});
	
	
})

.controller('SearchCtrl', function($scope,$state,$ionicHistory,$localstorage,NavService) {  // 인자값으로 service,factory 모듈 호출
	NavService.ec_all().then(function(res){
		console.log(res);
		$scope.el = res.data;
	});
	NavService.nc_all().then(function(res){
		console.log(res);
		$scope.nl = res.data;
	});
	
	$scope.goMain = function(kind,val){
		console.log(kind+" : "+val);
		//초기화
		$localstorage.set('code_cd','');
		$localstorage.set('keyword','');
		$localstorage.set('station_nm','');
		
		
		switch(kind){
			case 'keyword' : 
					$localstorage.set('keyword',val);
					$localstorage.set('code_cd','CG00'); //이색할인을 누를경우 키워드는 전체
					break;
			case 'code_cd' :
					$localstorage.set('code_cd',val);
					break;
		};
		console.log("kw = "+$localstorage.get('keyword'));
		$state.go('tab.search_inner');
	};
})
.controller('SearchInnerCtrl',function($scope,$state,$localstorage,EventSearchService,_Utile){
	
	$scope.slideChanged = function(index) {
		$scope.slideIndex = index;
		//$ionicSlideBoxDelegate.update(); 
	};
	$scope.back = function(){
		_Utile.back();
	}
	
	 /*탭 영역 */
	  var default_tab_path = 'templates/search/inc/';
	  $scope.tabList = [
	                    {tabTitle : '지역/키워드',active:true,url:'event-search.html'},
	                    {tabTitle : '수도권 지하철',active:false,url:'subway.html'}
	                    ];
	  $scope.tab = {
			  url : default_tab_path+$scope.tabList[0].url
	  }
	  
	  $scope.search_tab = function(item){
		  console.log(item);
		  resetActive();

		  if(!item.active){
			  $scope.tab.url = default_tab_path+item.url;
			  item.active = !item.acvite;
		  }
		  
		  console.log("path - "+$scope.tab.url);
		  
	  }
	  
	  function resetActive(){
		  for(var i in $scope.tabList){
			  $scope.tabList[i].active = false;
		  }
	  }
	 /* end 탭 영역 */
	$scope.data = {
			seachVal : ""
	};
	  
	$scope.data.seachVal = $localstorage.get('keyword') !=null?"#"+$localstorage.get('keyword'):"";
	EventSearchService.all().then(function(res) {
		console.log("EventSearchCtrl");
		console.log(res);
		$scope.eventList = res.data;
	});
	$scope.goMain = function() {
		console.log("kw"+$scope.data.seachVal);
		$localstorage.set('keyword',$scope.data.seachVal);
		$state.go('tab.main-list',{param:$localstorage.get('keyword')}, { reload: true });
	};
	
	$scope.kwAdd = function(kw){
		console.log(kw);
		$scope.data.seachVal = kw;
	}
})
	.controller('event-searchCtrl', function($scope,$state,esService){
		esService.local_kw().then(function(res){
			console.log('local_kw');
			console.log(res);
			$scope.local_kw = res.data;
			console.log($scope.local_kw);
		});
		esService.band_kw().then(function(res){
			console.log('band_kw');
			console.log(res);
			$scope.band_kw = res.data;
		});
				
	})
	.controller('SubwayCtrl', function($scope,$state,$localstorage,$ionicLoading,$timeout,SubwayService) {
		var subway_line = {
				'1호선' : '1',
				'2호선' : '2',
				'3호선' : '3',
				'4호선' : '4',
				'5호선' : '5',
				'6호선' : '6',
				'7호선' : '7',
				'8호선' : '8',
				'9호선' : '9',
				'인천1호선' : 'I',
				'경의/중앙선' : 'K',
				'분당선' : 'B',
				'공항철도' : 'A',
				'경춘선' : 'G',
				'신분당선' : 'S',
				'수인선' : 'SU',
		}
		$scope.line_code = subway_line;
		var subway_key = "4b6b6d4d6373657239305a6b415468"; // 보안 작업  해야함
		var subline_url = "SearchSTNBySubwayLineService";
		var subname_url = "SearchInfoBySubwayNameService";
		$scope.lineClick = function($event,line_code){
			$scope.loading = //로딩화면 생성
				$ionicLoading.show({
					content: 'Loading',
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 0
				});

			// 호선 요청 시 역명 스크롤 포지션 초기화.
			document.querySelector("#station-scroller").scrollTop = 0;				
						
			console.log($event);
			angular.element('.subway').find('.line_code span').removeClass('on');
			angular.element($event.target).addClass('on');
			
			var subwayObj = {
					key : subway_key,
					subline : subline_url,
					line_code : line_code
			};
			
			SubwayService.all(subwayObj).then(function(res){
				var result = res.data.SearchSTNBySubwayLineService.RESULT;
				var row = res.data.SearchSTNBySubwayLineService.row;
				console.log(res.data.SearchSTNBySubwayLineService.RESULT);
				console.log(res.data.SearchSTNBySubwayLineService.row);
				$timeout(function () {
	    		    $ionicLoading.hide();
	    		}, 500);
				$scope.subway_row = row;
				
				
				$scope.subwayClick = function(item){
					console.log(item);
					item = JSON.parse(item);
					console.log("지허철 = "+item.STATION_NM);
					 var path = "tab.main-list";
					//{"STATION_CD":"1957","STATION_NM":"주엽","LINE_NUM":"3","FR_CODE":"311"}
					$localstorage.set('station_nm',item.STATION_NM);
					$state.go(path,{param:item.STATION_NM}, { reload: true });
				}
			});
		}
	})
.controller('MainListCtrl', function($scope,$state,$window,$ionicNavBarDelegate,$localstorage,$ionicLoading,$timeout,$ionicPopup,toaster,MainList,ShopUtile) {
	$scope.noMoreItemsAvailable = false;
	console.log("$state.params");
	console.log($state.params);
	var ele = document.querySelector('.main-list-display');
	$scope.displayH = angular
		.element(document.querySelectorAll(".view-container"))
		.width()/2;
	console.log("displayW1 - "+angular
			.element(document.querySelectorAll(".view-container"))
			.width());
	console.log("displayW2 - "+$scope.displayH);
	$ionicNavBarDelegate.align('left');
	
	//getElementsByClassName
	var page_index = 0;  //index
	var max_page = 20;  //한번에 로드할 데이터 수
	
	
	//선택시 로드하는 문제 와 데이터가 없을시 계속 부르는 문제 수정해야함


	$scope.email = $localstorage.ask('user')?JSON.parse($localstorage.get("user")).email:"";
	$scope.code_cd = $localstorage.get('code_cd')!=null?$localstorage.get('code_cd'):"CG00";
	$scope.station_nm = $localstorage.get('station_nm')!=null?$localstorage.get('station_nm'):"";
	$scope.keyword = $localstorage.get('keyword')!=null?$localstorage.get('keyword'):"";
	$scope.order_list = $localstorage.get('order_list')!=null?$localstorage.get('order_list'):"TD_SHOP.SHOP_COUNT";
	console.log($scope.code_cd+":"+$scope.station_nm+":"+$scope.keyword);
	
	var loadMoreChk = true;
	
	$scope.loadMore = function() {
		page_index ++;
		
		var row_s = page_index!=1?(max_page*(page_index-1)+1):max_page*(page_index-1);
		var row_e = max_page*((page_index-1)+1);
		
		var  mainObj = {
				email : $scope.email,
				row_s : row_s,
				row_e : row_e,
				code_cd : $scope.code_cd,
				station_nm : $scope.station_nm,
				keyword : $scope.keyword,
				order_list : $scope.order_list  //TD_SVC.cnfm_dt CNT.TOTAL
				};
		if(loadMoreChk){  //
			MainList.all(mainObj).then(function(res){
				console.log("main-list");
				console.log(res);
				console.log(res.data.length);
				if($scope.all != null){
					for(i in res.data){
						$scope.all.push(res.data[i]);
					}
				}else{
					$scope.all = res.data;
				}

			    if ( $scope.all.length == 99 ) {
			      $scope.noMoreItemsAvailable = true;
			    }
			    
			    if(res.data.length <= 0){
			    	loadMoreChk = false;
			    	//ion-infinite-scroll
			    	angular
					.element(document.querySelectorAll("ion-infinite-scroll"))
					.attr("style","display:none");
			    	$scope.notData = "검색 결과가 없습니다.";
			    }else{
			    	$scope.notData = "";
			    }

			    $scope.$broadcast('scroll.infiniteScrollComplete');

			});
		}
		
	  };
	  
	  $scope.order_data = [
							{
								val : 'TD_SVC.cnfm_dt',
								txt : '최신순'
							},
							{
								val : 'TD_SHOP.SHOP_COUNT',
								txt : '조회순'
							},
							{
								val : 'CNT.TOTAL',
								txt : 'Total순'
							}
	                     ];
	 $scope.orderBtn = function() {
		var order_popup = $ionicPopup
				.show({
					templateUrl: 'templates/popup/order_list.html',
					title : '정렬',
					cssClass: 'order-popup',
					scope : $scope
				});
		order_popup.then(function(res) {
			console.log('Tapped!', res);
		});
		
		$scope.popup_close = function(){
			order_popup.close();
		};
		$scope.orderAction = function(orderVal){
			console.log(orderVal);
			order_popup.close();
			$localstorage.set("order_list",orderVal);
			$window.location.reload(true);
		};
	};
	  
	
	  $scope.shop_info = function(shop_id){
		  var path = "tab.main-list."+shop_id;
		  $state.go(path);
	  }
	  $scope.webLink = function(web_link){
		  console.log(web_link);
			if(web_link){
				//var link = 'http://'+web_link;
		        var link = (String(web_link).startsWith('http://') 
					|| String(web_link).startsWith('https://'))
				? web_link : 'http://'+web_link;
		        window.open(link, '_blank', 'transitionstyle=crossdissolve,toolbarposition=top');
			}else{
				toaster.pop('error'
						, "알림창"
						, "등록된 정보가 없습니다."
						, 2000);
			}
			return false;
	  }
	  $scope.point = function(type,item){
			var pointObj = {
					url : "",
					email : "",
					shop_id : "",
					sel_col : ""
			};
			switch(type){
				case 'zzim':
					if(!$localstorage.ask('user')){
						toaster.pop('error'
              				, "알림창"
              				, "로그인 이후 가능 합니다."
              				, 2000);
						//딜레이주자
						return;
					}
					
					$scope.loading = //로딩화면 생성
						$ionicLoading.show({
							content: 'Loading',
							animation: 'fade-in',
							showBackdrop: true,
							maxWidth: 200,
							showDelay: 0
						});
					
						var zzimObj = {
							email : JSON.parse($localstorage.get("user")).email,
							shop_id : item.shop_id
						};
					
						ShopUtile.zzim_len(zzimObj).then(function(res){
							console.log(res.data[0]);
							if(res.data[0].zzim_cnt < 5){
								ShopUtile.zzim(zzimObj).then(function(res){
									console.log(res);
									$timeout(function () {
										$ionicLoading.hide();
										item.zzim_on = !item.zzim_on;
										
										if(item.zzim_on){
											pointObj.url = "/service/zzimOn";
											pointObj.email = zzimObj.email;
											pointObj.shop_id = zzimObj.shopId;
											ShopUtile.point(pointObj).then(function(res){
												console.log("zzimOn");
												console.log(res);
											});
										}else{
											ShopUtile.zzim_del(zzimObj).then(function(res){
												console.log(res);
												console.log("zzim del");
											});
										}
									    
									}, 2000);
									
								});
							}else{
								$ionicLoading.hide();
								toaster.pop('error'
			              				, "알림창"
			              				, "20개 이상 등록할 수 없습니다."
			              				, 2000);
							}
						});
						
						
					break;
				case 'share':

					  var myPopup = $ionicPopup.show({
							 template: '<input type="password" ng-model="data.wifi">',
							 title: 'Enter Wi-Fi Password',
							 subTitle: 'Please use normal things',
							 scope: $scope,
							 buttons: [
							           { text: 'Cancel' },
							           {
							        	   text: '<b>Save</b>',
							        	   type: 'button-positive',
							        	   onTap: function(e) {
							        		   if (!$scope.data.wifi) {
							        			   //don't allow the user to close unless he enters wifi password
							        			   e.preventDefault();
							        		   } else {
							        			   return $scope.data.wifi;
							        		   }
							        	   }
							           },
							           ]
							   });
					  myPopup.then(function(res) {
						     console.log('Tapped!', res);
						   });
				  
					  
					pointObj.url = "/service/shopTotalCount";
					pointObj.shop_id = $stateParams.shopId;
					pointObj.sel_col = "share_count";
					ShopUtile.point(pointObj).then(function(res){
						console.log("share_count");
						console.log(res);
					});
					break;		
			}
		}
	  
	  
	 /* myPopup.close(); */

	
})
.controller('ShopDetailCtrl', function($scope, $state,$cordovaSocialSharing,$ionicPopup, $stateParams,$localstorage,$ionicLoading,$ionicHistory,$timeout, $compile,$ionicHistory,toaster,_Utile,ShopDetailService,ShopUtile) {
	console.log("crtl - "+$stateParams.shopId);
	//최근 본 목록 추가
	if($stateParams.shopId != null && $stateParams.shopId !=''){
		if($localstorage.get("recentTmp") != null  && $localstorage.get("recentTmp") != ''){
			console.log(recentChk());
			if(recentChk()){
				var recentTmp = $localstorage.get("recentTmp");
				recentTmp += ','+$stateParams.shopId;
				$localstorage.set("recentTmp",recentTmp);
				console.log("추가시 = "+$localstorage.get("recentTmp"));
			}
			
		}else{
			$localstorage.set("recentTmp",$stateParams.shopId);
		}
	}
	
	function recentChk(){
		var bool = false;
		var recentList = [];
		var n = 0;
		var maxRec = 19;
		recentList = $localstorage.get("recentTmp").split(',');
		n = recentList.indexOf($stateParams.shopId);
		console.log(recentList);
		console.log(n);
		if(n < 0){
			bool = true;
			//특정 갯수 이상시 뒤에서부터 땡기
			console.log(recentList.length +">"+maxRec);
			if(recentList.length > maxRec){
				recentList = recentList.slice(1);
				$localstorage.set('recentTmp',recentList.toString());
				console.log($localstorage.get('recentTmp'));
			}
		}
		
		
		return bool;
	}

	var shopDetailObj = {
			shop_id : $stateParams.shopId,
			email : $localstorage.ask('user')?JSON.parse($localstorage.get('user')).email:""
	};
	ShopDetailService.all(shopDetailObj).then(function(res){
		$scope.data = res.data[0];
		console.log(res);
		
		$scope.slideChanged = function(index) {
			$scope.slideIndex = index;
			//$ionicSlideBoxDelegate.update(); 
		};
		$scope.bannerList = res.data[0].shop_img.split(",");
		
		$scope.total = res.data[0].rec_cnt+
							res.data[0].share_count+
							res.data[0].call_count+
							res.data[0].shop_count;
		//$scope.contents = _Utile.trim(res.data[0].contents);
		$scope.contents = res.data[0].contents.replace(/\s+/, " ");
		var mt = res.data[0].contents.indexOf('\n') > 0
				?res.data[0].contents.indexOf('\n') :res.data[0].contents.length;
		$scope.contents_s = res.data[0].contents.substring(0,mt);
		
		$scope.kwList = res.data[0].keyword.length<0?"":res.data[0].keyword.split(",");
		
		//MAP
		var shop_addr = res.data[0].shop_addr;
		initialize(res.data[0].shop_nm,shop_addr.substring(0,shop_addr.indexOf("(")));
		
		//점수 point
		$scope.point = function(type){
			var pointObj = {
					url : "",
					email : "",
					shop_id : "",
					sel_col : ""
			};
			switch(type){
				case 'zzim':
					if(!$localstorage.ask('user')){
						toaster.pop('error'
                				, "알림창"
                				, "로그인 이후 가능 합니다."
                				, 2000);
						//딜레이주자
						return;
					}
					
					$scope.loading = //로딩화면 생성
						$ionicLoading.show({
							content: 'Loading',
							animation: 'fade-in',
							showBackdrop: true,
							maxWidth: 200,
							showDelay: 0
						});
				 
					var zzimObj = {
						email : JSON.parse($localstorage.get("user")).email,
						shop_id : $stateParams.shopId
					};
					ShopUtile.zzim(zzimObj).then(function(res){
						console.log(res);
						$timeout(function () {
							$ionicLoading.hide();
							$scope.data.zzim_on = !$scope.data.zzim_on;
							
							
							if($scope.data.zzim_on){
								pointObj.url = "/service/zzimOn";
								pointObj.email = zzimObj.email;
								pointObj.shop_id = $stateParams.shopId;
								ShopUtile.point(pointObj).then(function(res){
									console.log("zzimOn");
									console.log(res);
								});
							}else{
								ShopUtile.zzim_del(zzimObj).then(function(res){
									console.log(res);
									console.log("zzim del");
								});
							}
						    
						}, 2000);
						
					});

					
					break;
				case 'share':
					
					var sharePopup = $ionicPopup.show({
						templateUrl: 'templates/popup/share.html',
					    title: '공유',
					    cssClass : 'share-popup',
					    scope: $scope,
					    buttons: [
					      { text: 'Cancel' }
					    			]
					  });
					
					$scope.share_facebook = function(){
						var message,image,link;
						message = "sadfsadf";
						image = "http://52.79.78.117/img/1150/BB/BB_1150_2_20160720171851.JPEG";
						link = "https://play.google.com/store/apps/details?id=com.amazon.difi";

					/*	$cordovaSocialSharing
				    	.shareViaFacebook("sadfsf", image, "https://play.google.com/store/apps/details?id=com.amazon.difi")
				    	.then(function(result) {
				    		// Success!
				    	}, function(err) {
				    		// An error occurred. Show a message to the user
				    	});
						*/
						$cordovaSocialSharing
							.shareViaFacebook("facebook", message, image, link)
							.then(function(result) {
								$cordovaSocialSharing.shareViaFacebook(message, image, link);
							}, function(error) {
								console.log("Cannot share on Whatsapp");
							});
					}
					
					/*$cordovaSocialSharing
				    	.shareViaFacebook(message, image, link)
				    	.then(function(result) {
				    		// Success!
				    	}, function(err) {
				    		// An error occurred. Show a message to the user
				    	});
					*/
					return;
					
					pointObj.url = "/service/shopTotalCount";
					pointObj.shop_id = $stateParams.shopId;
					pointObj.sel_col = "share_count";
					ShopUtile.point(pointObj).then(function(res){
						console.log("share_count");
						console.log(res);
					});
					break;
				case 'call':
					pointObj.url = "/service/shopTotalCount";
					pointObj.shop_id = $stateParams.shopId;
					pointObj.sel_col = "call_count";
					ShopUtile.point(pointObj).then(function(res){
						console.log("call_count");
						console.log(res);
					});
					break;			
			}
		}
	});
	
	$scope.goKwSearch = function(item){
		console.log(item);
		//초기화
		item = item.replace(/(^\s*)|(\s*$)/g,"");
		$localstorage.set('code_cd','');
		$localstorage.set('keyword',item);
		$localstorage.set('station_nm','');
		$state.go('tab.main-list',{param:item});
		$ionicHistory.clearCache();
	};
	$scope.webLink = function(e){
		console.log(e);
		if($scope.data.web_link){
			//var link = 'http://'+$scope.data.web_link;
	        var link = (String($scope.data.web_link).startsWith('http://') 
					|| String($scope.data.web_link).startsWith('https://'))
				? $scope.data.web_link : 'http://'+$scope.data.web_link;
	        window.open(link, '_blank', 'transitionstyle=crossdissolve,toolbarposition=top');
		}else{
			toaster.pop('error'
					, "알림창"
					, "등록된 정보가 없습니다."
					, 2000);
		}
		return false;
	}
	
	
	$scope.back = function(){
		_Utile.back();
	}
	
	//map
	function initialize(shop_nm,shop_addr) {
		console.log("shop_addr - "+shop_addr);
		//http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=ko&address=인코딩된주소
		ShopDetailService.latlng(shop_addr).then(function(res){
			console.log(res);
			var lat = res.data.results[0].geometry.location.lat;
            var lng = res.data.results[0].geometry.location.lng;
			console.log(lat+","+lng);
			var myLatlng = new google.maps.LatLng(lat,lng);
			
			var mapOptions = {
				center: myLatlng,
				zoom: 16,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("map"),mapOptions);
		
			//Marker + infowindow + angularjs compiled ng-click
			
			/*var contentString = "<div><a ng-click='clickTest()'>Click me!</a></div>";*/
			var contentString = "<div>"+shop_nm+"</div>";
			var compiled = $compile(contentString)($scope);
		
			var infowindow = new google.maps.InfoWindow({
				content: compiled[0]
			});
		
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				title: 'Uluru (Ayers Rock)'
			});
		
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			});
		
			$scope.map = map;
			
		});
		
	}
	
	//google.maps.event.addDomListener(window, 'load', initialize);

})

.controller('RecentCtrl', function($scope,$localstorage,_Utile,Recent) {
	/*$scope.$on('$ionicView.enter', function(e) {});*/
	console.log("RecentCtrl");
	var recAllObj = {
			shops : $localstorage.get("recentTmp")
	};

	Recent.all(recAllObj).then(function(res){
		console.log("rescent lsit");
		console.log(res.data);
		for(var i in res.data){
			if(res.data[i].shop_img == null){
				res.data[i].local_img = Math.floor((Math.random()*4)+1);
			}
		}
		$scope.all = res.data;
	});
	$scope.recentDel = function(index){
		Recent.remove($scope.all[index].shop_id).then(function(res){
			for(var i in res.data){
				if(res.data[i].shop_img == null){
					res.data[i].local_img = Math.floor((Math.random()*4)+1);
				}
			}
			$scope.all = res.data;
		});
	};
	$scope.allDel = function(){
		$localstorage.set("recentTmp","");
		$scope.all = null;
	};
	
	

})
.controller('ZzimsCtrl', function($scope,$state, $localstorage,Zzims,toaster,_Utile) {
	console.log("ZzimsCtrl");
	
	$scope.$on("$ionicView.beforeEnter", function(event, data){
	   // handle event
	   console.log("State Params: ", data.stateParams);
	});

	$scope.$on("$ionicView.enter", function(event, data){
	   // handle event
	   console.log("State Params: ", data.stateParams);
	   if($localstorage.ask('user')){
			Zzims.all().then(function(res){
				console.log("Zzims lsit");
				for(var i in res.data){
					if(res.data[i].shop_img == null){
						res.data[i].local_img = Math.floor((Math.random()*4)+1);
					}
				}
				$scope.all = res.data;
			});
			
			$scope.zzimsDel = function(index){
				var removeObj = {
					shop_id : $scope.all[index].shop_id,
					email : JSON.parse($localstorage.get("user")).email
				};
				Zzims.remove(removeObj).then(function(res){
					console.log(res);
					Zzims.all().then(function(res){  //지운후 새로 불러온다
						console.log("Zzims lsit");
						console.log(res);
						for(var i in res.data){
							if(res.data[i].shop_img == null){
								res.data[i].local_img = Math.floor((Math.random()*4)+1);
							}
						}
						$scope.all = res.data;
					});
				});
				
			};
		}else{
			//로그인 안한상태 
			toaster.pop('error'
					, "알림창"
					, "로그인 이후 가능 합니다."
					, 2000);
		}
		$scope.getRandomNum = function(){
		    return Math.floor((Math.random()*4)+1);
		};
	});

	$scope.$on("$ionicView.afterEnter", function(event, data){
	   // handle event
	   console.log("State Params: ", data.stateParams);
	});

	
})

.controller('SettingCtrl', function($scope,$state,$ionicHistory,$ionicPopup, $localstorage,toaster,_Utile) {
	console.log("SettingCtrl");
	$scope.version = $localstorage.get("version");
	$scope.logout = function(){
		var confirmPopup = $ionicPopup
			.confirm({
				template : '<b class="b1">Bo</b><b class="b2">go</b><b class="b3">ga</b>에서 로그아웃 하시겠습니까?',
				cssClass : 'logout-popup',
				buttons: [{
				    text: '취소',
				    type: 'button-default',
				    onTap: function(e) {
				    	confirmPopup.close();
				    }
				  }, {
				    text: '확인',
				    type: 'button-positive',
				    onTap: function(e) {
				    	return true;
				    }
				  }]
			});
		confirmPopup.then(function(res) {
			console.log(res);
			if (res) {
				$localstorage.set("user",null);
				$state.go('tab.main-list',{param:""});
				$ionicHistory.clearCache();
			} else {
				console.log('You are not sure');
			}
		});
	};
	$scope.pwSetting = function(){
		if($localstorage.ask('user')){
			$state.go('pw_change');
		}else{
			toaster.pop('error'
					, "알림창"
					, "로그인 이후 가능 합니다."
					, 2000);
		}
		
	};
	
	$scope.back = function(){
		_Utile.back();
	}

})
.controller('PwChangeCtrl', function($scope,$state,$ionicHistory,$ionicPopup, $localstorage,$ionicLoading,$timeout,toaster,_Utile,PwChangeService){
	console.log('PwChangeCtrl');
	var user = JSON.parse($localstorage.get("user"));
	$scope.data = {
			password : "",
			password_chk : ""
	};
	
	$scope.pwc_submit = function(){
		var blank_pattern = /[\s]/g;
		console.log($scope.data);
		if($scope.data.password == ""){
			toaster.pop('error'
					, "알림창"
					, "비밀번호를 입력해주세요"
					, 2000);
			return ;
		}else if($scope.data.password_chk == ""){
			toaster.pop('error'
					, "알림창"
					, "비밀번호 확인이 틀립니다."
					, 2000);
			return;
		}else if($scope.data.password != $scope.data.password_chk){
			toaster.pop('error'
					, "알림창"
					, "비밀번호를 다시한번 확인해주세요"
					, 2000);
			return;
		}else if(blank_pattern.test($scope.data.password) == true){
			toaster.pop('error'
					, "알림창"
					, "공백은 사용할 수 없습니다."
					, 2000);
			return;
		}
		
		var pwcObj = {
				email : user.email,
				password : $scope.data.password
		};
		$scope.loading = //로딩화면 생성
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
		PwChangeService.pwc(pwcObj).success(function(res) {
			$timeout(function () {
				$ionicLoading.hide();
				toaster.pop('success'
						, "알림창"
						, "비밀번호 변경 완료"
						, 2000);
			});
			$localstorage.set("user",null);
			$state.go('tab.main-list',{param:""});
			$ionicHistory.clearCache();
			
        })
        .error(function(res) {
        	$timeout(function () {
				$ionicLoading.hide();
				toaster.pop('error'
						, "알림창"
						, "비밀번호 변경 실패"
						, 2000);
			});
        });
		
	}
	
	$scope.back = function(){
		_Utile.back();
	}
})
.controller('CustomerCenterCtrl', function($scope,$state, $localstorage,_Utile) {
	console.log("CustomerCenterCtrl");
	$scope.back = function(){
		_Utile.back();
	}
	
	$scope.accessterms = function(){
		console.log("sdf");
		$state.go('accessterms');
	}
	
	$scope.guide = function(){
		console.log("guide");
		$state.go('guide');
	}
	
	$scope.inquiry = function(){
		console.log("sdf");
		$state.go('inquiry');
	}
})
.controller('GuideCtrl',function($scope,$state,_Utile){
	console.log("GuideCtrl");
	$scope.back = function(){
		_Utile.back();
	}
})
.controller('InquiryCtrl',function($scope,$state,_Utile){
	console.log("InquiryCtrl");
	$scope.back = function(){
		_Utile.back();
	}
})
.controller('AccesstermsCtrl', function($scope, $localstorage,_Utile) {
	console.log("AccesstermsCtrl");
	$scope.back = function(){
		_Utile.back();
	}
	
	/*카테고리 탭 영역 */
	  var default_tab_path = 'templates/popup/inc/';
	  $scope.tabList = [
	                    {tabTitle : '서비스 이용약관',active:true,url:'access.html'},
	                    {tabTitle : '개인정보 취급방침',active:false,url:'personal.html'}
	                    ];
	  $scope.tab = {
			  url : 'templates/popup/inc/'+$scope.tabList[0].url
	  };
	  $scope.tabAction = function(item){
		  console.log(item);
		  resetActive();

		  if(!item.active){
			  $scope.tab.url = default_tab_path+item.url;
			  item.active = !item.acvite;
		  };
		  
	  };
	  
	  function resetActive(){
		  for(var i in $scope.tabList){
			  $scope.tabList[i].active = false;
		  }
	  };

})

/*directive*/

.directive('hideTabs', function($rootScope) {
	return {
		restrict: 'A',
		link: function($scope, $el, $attr) {
			var tmpAttr;
			switch($attr.hideTabs){
				case 'show' :
					tmpAttr = '';
					break;
				case 'hide' :
					tmpAttr = 'tabs-item-hide';
					break;
				default :
					tmpAttr='';
					
			}
			$rootScope.hideTabs = tmpAttr;
			$scope.$on('$destroy', function() {
				$rootScope.hideTabs = '';
			});
		}
	};
})
.directive('numbersOnly', function($rootScope) {
	return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
})
.directive('parseBool', function($rootScope) {
	return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromValue(val) {
            	console.log("parseBool");
            	console.log(val);
            	var bool = null;
            	if(val){
            		bool = (val == 'true' ? true : false);
            	}
            	return bool;
            }            
            ngModelCtrl.$parsers.push(fromValue);
        }
    };
})
;
