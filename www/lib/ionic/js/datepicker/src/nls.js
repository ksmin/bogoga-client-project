(function() {

  'use strict';

  angular
  .module('ionic-datepicker')
  .service('DatepickerNls', function () {

    var nls = {
      'en-us': {
        weekdays: [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ],
        months: [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]
      },
      'pt-br': {
        weekdays: [ 'Domingo', 'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-Feira', 'Sexta-Feira', 'Sábado' ],
        months: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ]
      },
      'en-ko': {
    	  weekdays: [ '일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일' ],
          months: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ]
        }
    };

    this.getWeekdays = function(locale) {
      return this._getNls(locale).weekdays;
    };

    this.getMonths = function(locale) {
      return this._getNls(locale).months;
    };

    this._getNls = function(locale) {
      /*return nls[locale] || nls['en-ko'];*/
    	return nls['en-ko'];
    };

  });

})();
